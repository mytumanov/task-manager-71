package ru.mtumanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class ApplicationVersionListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String getArgument() {
        return "-v";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show program version";
    }

    @Override
    @NotNull
    public String getName() {
        return "version";
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

}
