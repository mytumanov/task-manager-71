package ru.mtumanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.server.ServerAboutRq;
import ru.mtumanov.tm.dto.request.server.ServerVersionRq;
import ru.mtumanov.tm.dto.response.server.ServerAboutRs;
import ru.mtumanov.tm.dto.response.server.ServerVersionRs;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutRs getAbout(@NotNull ServerAboutRq serverAboutRequest);

    @NotNull
    @WebMethod
    ServerVersionRs getVersion(@NotNull ServerVersionRq serverVersionRequest);

}
