package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@NoArgsConstructor
public abstract class AbstractTaskRs extends AbstractResultRs {

    @Nullable
    @Getter
    @Setter
    private TaskDTO task;

    protected AbstractTaskRs(@Nullable final TaskDTO task) {
        this.task = task;
    }

    protected AbstractTaskRs(@NotNull final Throwable err) {
        super(err);
    }

}