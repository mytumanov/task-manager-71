package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.response.AbstractRs;

import java.util.List;

@Getter
@NoArgsConstructor
public class TaskListByProjectIdRs extends AbstractRs {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListByProjectIdRs(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}