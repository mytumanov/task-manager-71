package ru.mtumanov.tm.exception.user;

public class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("ERROR! User not found!");
    }

}
