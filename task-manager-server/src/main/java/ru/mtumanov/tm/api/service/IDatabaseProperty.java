package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2DdlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseSecondLevelCache();

    @NotNull
    String getDatabaseFactoryClass();

    @NotNull
    String getDatabaseUseQueryCache();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseConfigFilePath();

    @NotNull
    String getDatabaseUseMinPuts();

}
