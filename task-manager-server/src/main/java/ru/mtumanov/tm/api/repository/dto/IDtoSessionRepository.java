package ru.mtumanov.tm.api.repository.dto;

import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.SessionDTO;

@Repository
public interface IDtoSessionRepository extends IDtoUserOwnedRepository<SessionDTO> {

}
