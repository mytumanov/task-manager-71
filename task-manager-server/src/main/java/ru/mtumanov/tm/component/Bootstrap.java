package ru.mtumanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.endpoint.*;
import ru.mtumanov.tm.api.service.*;
import ru.mtumanov.tm.api.service.dto.*;
import ru.mtumanov.tm.endpoint.AbstractEndpoint;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Getter
    @Autowired
    private IDtoProjectTaskService projectTaskService;

    @NotNull
    @Getter
    @Autowired
    private IDtoProjectService projectService;

    @NotNull
    @Getter
    @Autowired
    private IDtoTaskService taskService;

    @NotNull
    @Getter
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IDtoSessionService sessionService;

    @NotNull
    @Getter
    @Autowired
    private IDtoUserService userService;

    @NotNull
    @Getter
    @Autowired
    private IAuthService authService;

    @NotNull
    @Getter
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private Backup backup;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        loggerService.debug("url: " + url);
        Endpoint.publish(url, endpoint);
    }

    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(fileName), pid.getBytes());
        } catch (@NotNull final IOException e) {
            loggerService.error(e);
        }
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        initEndpoints();
        try {
            loggerService.initJmsLogger();
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initDemo();
        initPID();
    }

    private void initDemo() {
        try {
            userService.create("COOL_USER", "cool", Role.ADMIN);
            userService.create("NOT_COOL_USER", "Not Cool", "notcool@email.ru");
            userService.create("123qwe!", "vfda4432!", "vfda4432@email.ru");
        } catch (final Exception e) {
            System.out.println("ERROR! Fail to initialize test data.");
            System.out.println(e.getMessage());
        }
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
    }

}
