package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.service.model.IProjectService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.field.StatusNotSupportedException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.User;

import javax.transaction.Transactional;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @NotNull
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull Optional<User> userOptional = getUserRepository().findById(userId);
        if (!userOptional.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + userId);
        @NotNull User user = userOptional.get();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);

        repository.save(project);
        return project;
    }

    @Override
    @NotNull
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull final Project project = repository.findByUserIdAndId(userId, id);
        project.setName(name);
        project.setDescription(description);

        repository.save(project);
        return project;
    }

    @Override
    @NotNull
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (status == null)
            throw new StatusNotSupportedException();

        @NotNull final Project project = repository.findByUserIdAndId(userId, id);
        project.setStatus(status);
        repository.save(project);
        return project;
    }

}
