package ru.mtumanov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.model.CustomUser;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/project/create")
    public String create(@AuthenticationPrincipal final CustomUser user) {
        projectService.addByUserId(new ProjectDTO("Project Name", "Project description", Status.NOT_STARTED), user.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@AuthenticationPrincipal final CustomUser user, @PathVariable("id") @NotNull final String id) {
        projectService.removeByIdAndUserId(id, user.getUserId());
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@AuthenticationPrincipal final CustomUser user, @ModelAttribute("project") @NotNull final ProjectDTO project) {
        project.setUserId(user.getUserId());
        projectService.update(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@AuthenticationPrincipal final CustomUser user, @PathVariable("id") @NotNull final String id) {
        @Nullable final ProjectDTO project = projectService.findByIdAndUserId(id, user.getUserId());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Status[] getStatuses() {
        return Status.values();
    }

}
