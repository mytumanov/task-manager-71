package ru.mtumanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.model.CustomUser;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAllByUserId(user.getUserId()));
        modelAndView.addObject("projectRepository", projectService);
        return modelAndView;
    }

}
