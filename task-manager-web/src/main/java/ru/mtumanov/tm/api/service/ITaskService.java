package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskService {

    void clear();

    @NotNull
    TaskDTO addByUserId(@Nullable final TaskDTO task, @Nullable final String userId);

    @NotNull
    TaskDTO update(@Nullable final TaskDTO task);

    @Nullable
    List<TaskDTO> findAll();

    @Nullable
    List<TaskDTO> findAllByUserId(@Nullable final String userId);

    void remove(@Nullable final TaskDTO task);

    void removeByUserId(@Nullable final String userId);

    void removeById(@Nullable final String id);

    void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId);

    @Nullable
    TaskDTO findById(@Nullable final String id);

    @Nullable
    TaskDTO findByIdAndUserId(@Nullable final String id, @Nullable final String userId);

}
