package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/task")
public interface ITaskRestEndpoint {

    @Nullable
    @WebMethod
    @GetMapping
    Collection<TaskDTO> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/{id}")
    TaskDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id);

    @NotNull
    @WebMethod
    @PostMapping
    TaskDTO create(@WebParam(name = "task", partName = "task") @RequestBody @Nullable final TaskDTO task);

    @NotNull
    @WebMethod
    @PutMapping
    TaskDTO update(@WebParam(name = "task", partName = "task") @RequestBody @Nullable final TaskDTO task);

    @WebMethod
    @DeleteMapping
    void delete();

    @WebMethod
    @DeleteMapping
    void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id);

}
