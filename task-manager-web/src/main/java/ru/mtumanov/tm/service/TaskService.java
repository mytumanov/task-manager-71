package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO addByUserId(@Nullable final TaskDTO task, @Nullable final String userId) {
        if (task == null)
            throw new IllegalArgumentException();
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException("");
        task.setUserId(userId);
        return taskRepository.save(task);
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO update(@Nullable final TaskDTO task) {
        if (task == null)
            throw new IllegalArgumentException();
        return taskRepository.save(task);
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Nullable
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException("");
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final TaskDTO task) {
        if (task == null)
            throw new IllegalArgumentException();
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException("");
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null)
            throw new IllegalArgumentException();
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException("");
        taskRepository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Nullable
    @Transactional
    public TaskDTO findById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    @Transactional
    public TaskDTO findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null)
            throw new IllegalArgumentException();
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException("");
        return taskRepository.findByIdAndUserId(id, userId);
    }

}
