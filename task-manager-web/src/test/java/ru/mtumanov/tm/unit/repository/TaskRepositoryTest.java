package ru.mtumanov.tm.unit.repository;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import lombok.SneakyThrows;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.config.WebApplicationConfiguration;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.marker.UnitCategory;
import ru.mtumanov.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        WebApplicationConfiguration.class
})
public class TaskRepositoryTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Project 1", "Project 1 desc");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Project 2", "Project 2 desc");

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
    }


    @Test
    @SneakyThrows
    public void findAllTest() {
        List<TaskDTO> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskRepository.findByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdAndIdTest() {
        taskRepository.deleteByIdAndUserId(task1.getId(), UserUtil.getUserId());
        Assert.assertNull(taskRepository.findByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

}
