package ru.mtumanov.tm.unit.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import ru.mtumanov.tm.config.WebApplicationConfiguration;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        WebApplicationConfiguration.class
})
public class ProjectEndpointTest {

    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/project";

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private Collection<ProjectDTO> projects = new ArrayList<>();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        @NotNull ProjectDTO project1 = new ProjectDTO("Project 1", "Project 1 desc", Status.NOT_STARTED);
        @NotNull ProjectDTO project2 = new ProjectDTO("Project 2", "Project 2 desc", Status.IN_PROGRESS);
        @NotNull ProjectDTO project3 = new ProjectDTO("Project 3", "Project 3 desc", Status.COMPLETED);
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        save(project1);
        save(project2);
        save(project3);
    }

    @After
    @SneakyThrows
    public void clear() {
        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void save(@NotNull final ProjectDTO project) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private ProjectDTO findById(@NotNull final String id) {
        @NotNull String url = BASE_URL + "/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDTO.class);
    }

    @SneakyThrows
    private List<ProjectDTO> findAll() {
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, ProjectDTO[].class));
    }

    @Test
    public void findByIdTest() {
        for (ProjectDTO project : projects) {
            Assert.assertEquals(project.getId(), findById(project.getId()).getId());
        }
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(projects.size(), findAll().size());
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        for (ProjectDTO project : projects) {
            @NotNull String url = BASE_URL + "/" + project.getId();
            mockMvc.perform(MockMvcRequestBuilders.delete(url)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk());
            Assert.assertNull(findById(project.getId()));
        }
    }

    @Test
    public void createTest() {
        @NotNull ProjectDTO projectToCreate = new ProjectDTO("Project test", "Project test desc", Status.NOT_STARTED);
        save(projectToCreate);
        Assert.assertEquals(projectToCreate.getId(), findById(projectToCreate.getId()).getId());
    }

}
