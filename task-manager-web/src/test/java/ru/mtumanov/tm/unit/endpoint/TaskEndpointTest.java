package ru.mtumanov.tm.unit.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import ru.mtumanov.tm.config.WebApplicationConfiguration;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        WebApplicationConfiguration.class
})
public class TaskEndpointTest {

    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/task";

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private Collection<TaskDTO> tasks = new ArrayList<>();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        @NotNull TaskDTO project1 = new TaskDTO("task 1", "task 1 desc");
        @NotNull TaskDTO project2 = new TaskDTO("task 2", "task 2 desc");
        @NotNull TaskDTO project3 = new TaskDTO("task 3", "task 3 desc");
        tasks.add(project1);
        tasks.add(project2);
        tasks.add(project3);
        save(project1);
        save(project2);
        save(project3);
    }

    @After
    @SneakyThrows
    public void clear() {
        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void save(@NotNull final TaskDTO task) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private TaskDTO findById(@NotNull final String id) {
        @NotNull String url = BASE_URL + "/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDTO.class);
    }

    @SneakyThrows
    private List<TaskDTO> findAll() {
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
    }

    @Test
    public void findByIdTest() {
        for (TaskDTO task : tasks) {
            Assert.assertEquals(task.getId(), findById(task.getId()).getId());
        }
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(tasks.size(), findAll().size());
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        for (TaskDTO task : tasks) {
            @NotNull String url = BASE_URL + "/" + task.getId();
            mockMvc.perform(MockMvcRequestBuilders.delete(url)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk());
            Assert.assertNull(findById(task.getId()));
        }
    }

    @Test
    public void createTest() {
        @NotNull TaskDTO taskToCreate = new TaskDTO("task test", "task test desc");
        save(taskToCreate);
        Assert.assertEquals(taskToCreate.getId(), findById(taskToCreate.getId()).getId());
    }
}
