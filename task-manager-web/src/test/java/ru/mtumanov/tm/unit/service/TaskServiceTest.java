package ru.mtumanov.tm.unit.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.config.WebApplicationConfiguration;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.marker.UnitCategory;
import ru.mtumanov.tm.util.UserUtil;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        WebApplicationConfiguration.class
})
public class TaskServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Project 1", "Project 1 desc");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Project 2", "Project 2 desc");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Project 3", "Project 3 desc");

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.addByUserId(task1, UserUtil.getUserId());
        taskService.addByUserId(task2, UserUtil.getUserId());
    }

    @After
    public void cleanByUserIdTest() {
        taskService.removeByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdAndUserId() {
        Assert.assertNotNull(taskService.findByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserId() {
        @NotNull final List<TaskDTO> projects = new ArrayList<>();
        projects.add(task1);
        taskService.removeByIdAndUserId(task1.getId(), UserUtil.getUserId());
        Assert.assertNull(taskService.findByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeAllByUserId() {
        taskService.removeByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void writeList() {
        taskService.addByUserId(task3, UserUtil.getUserId());
        Assert.assertEquals(task3.getName(), taskService.findById(task3.getId()).getName());
        task3.setName(UUID.randomUUID().toString());
        taskService.update(task3);
        Assert.assertEquals(task3.getName(), taskService.findById(task3.getId()).getName());
    }

}
