package ru.mtumanov.tm.unit.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.config.WebApplicationConfiguration;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.marker.UnitCategory;
import ru.mtumanov.tm.util.UserUtil;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        WebApplicationConfiguration.class
})
public class ProjectServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Project 1", "Project 1 desc", Status.NOT_STARTED);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Project 2", "Project 2 desc", Status.IN_PROGRESS);

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Project 3", "Project 3 desc", Status.IN_PROGRESS);

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.addByUserId(project1, UserUtil.getUserId());
        projectService.addByUserId(project2, UserUtil.getUserId());
    }

    @After
    public void cleanByUserIdTest() {
        projectService.removeByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdAndUserId() {
        Assert.assertNotNull(projectService.findByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserId() {
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        projects.add(project1);
        projectService.removeByIdAndUserId(project1.getId(), UserUtil.getUserId());
        Assert.assertNull(projectService.findByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeAllByUserId() {
        projectService.removeByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void writeList() {
        projectService.addByUserId(project3, UserUtil.getUserId());
        Assert.assertEquals(project3.getName(), projectService.findById(project3.getId()).getName());
        project3.setName(UUID.randomUUID().toString());
        projectService.update(project3);
        Assert.assertEquals(project3.getName(), projectService.findById(project3.getId()).getName());
    }

}
