package ru.mtumanov.tm.integration.rest;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.marker.IntegrationCategory;
import ru.mtumanov.tm.model.Result;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    final static List<TaskDTO> tasks = new ArrayList<>();
    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/task";
    @NotNull
    private final static HttpHeaders header = new HttpHeaders();
    @Nullable
    private static String sessionId;

    @BeforeClass
    public static void init() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<TaskDTO> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Before
    public void initData() {
        @NotNull TaskDTO task1 = new TaskDTO("task 1", "task 1 desc");
        @NotNull TaskDTO task2 = new TaskDTO("task 2", "task 2 desc");
        @NotNull TaskDTO task3 = new TaskDTO("task 3", "task 3 desc");
        sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(task1, header));
        sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(task2, header));
        sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(task3, header));
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
    }

    @After
    public void clearData() {
        @NotNull final ResponseEntity<TaskDTO> response = sendRequest(BASE_URL, HttpMethod.DELETE, new HttpEntity<>(header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    public void create() {
        @NotNull final TaskDTO expectedProject = new TaskDTO("task test", "task test desc");
        @NotNull final ResponseEntity<TaskDTO> response = sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(expectedProject, header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final TaskDTO actualProject = response.getBody();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(expectedProject.getId(), actualProject.getId());
    }

    @Test
    public void findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<TaskDTO[]> response = restTemplate.exchange(BASE_URL, HttpMethod.GET, new HttpEntity<>(header), TaskDTO[].class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotEquals(response.getBody().length, 0);
        Assert.assertEquals(response.getBody().length, tasks.size());
    }

    @Test
    public void findById() {
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String url = BASE_URL + "/" + task.getId();
            @NotNull final ResponseEntity<TaskDTO> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
            Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
            @Nullable final TaskDTO actualProject = response.getBody();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(task.getId(), actualProject.getId());
        }
    }

    @Test
    public void deleteById() {
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String url = BASE_URL + "/" + task.getId();
            @NotNull final ResponseEntity<TaskDTO> responseDete = sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
            Assert.assertEquals(responseDete.getStatusCode(), (HttpStatus.OK));

            @NotNull final ResponseEntity<TaskDTO> responseFind = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
            Assert.assertNull(responseFind.getBody());
        }
    }

}
