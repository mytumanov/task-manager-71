package ru.mtumanov.tm.integration.rest;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.marker.IntegrationCategory;
import ru.mtumanov.tm.model.Result;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    final static List<ProjectDTO> projects = new ArrayList<>();
    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/project";
    @NotNull
    private final static HttpHeaders header = new HttpHeaders();
    @Nullable
    private static String sessionId;

    @BeforeClass
    public static void init() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<ProjectDTO> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Before
    public void initData() {
        @NotNull ProjectDTO project1 = new ProjectDTO("Project 1", "Project 1 desc", Status.NOT_STARTED);
        @NotNull ProjectDTO project2 = new ProjectDTO("Project 2", "Project 2 desc", Status.IN_PROGRESS);
        @NotNull ProjectDTO project3 = new ProjectDTO("Project 3", "Project 3 desc", Status.COMPLETED);
        sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(project1, header));
        sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(project2, header));
        sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(project3, header));
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
    }

    @After
    public void clearData() {
        @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(BASE_URL, HttpMethod.DELETE, new HttpEntity<>(header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    public void create() {
        @NotNull final ProjectDTO expectedProject = new ProjectDTO("Project test", "Project test desc", Status.NOT_STARTED);
        @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(BASE_URL, HttpMethod.POST, new HttpEntity<>(expectedProject, header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final ProjectDTO actualProject = response.getBody();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(expectedProject.getId(), actualProject.getId());
    }

    @Test
    public void findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDTO[]> response = restTemplate.exchange(BASE_URL, HttpMethod.GET, new HttpEntity<>(header), ProjectDTO[].class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotEquals(response.getBody().length, 0);
        Assert.assertEquals(response.getBody().length, projects.size());
    }

    @Test
    public void findById() {
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final String url = BASE_URL + "/" + project.getId();
            @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
            Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
            @Nullable final ProjectDTO actualProject = response.getBody();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(project.getId(), actualProject.getId());
        }
    }

    @Test
    public void deleteById() {
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final String url = BASE_URL + "/" + project.getId();
            @NotNull final ResponseEntity<ProjectDTO> responseDete = sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
            Assert.assertEquals(responseDete.getStatusCode(), (HttpStatus.OK));

            @NotNull final ResponseEntity<ProjectDTO> responseFind = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
            Assert.assertNull(responseFind.getBody());
        }
    }

}
